package com.kenpritchard;

import static org.junit.Assert.assertFalse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PipelinesApplicationTests {

	@Test
	public void contextLoads() {
	}

	@Test
	public void testFalse() {
		assertFalse(false);
	}

}
